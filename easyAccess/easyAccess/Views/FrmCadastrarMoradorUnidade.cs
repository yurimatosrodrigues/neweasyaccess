﻿using easyAccess.Controller;
using System;
using System.Windows.Forms;

namespace easyAccess
{
    public partial class FrmCadastrarMoradorUnidade : Form
    {                
                
        ClsMoradorUnidade clsMoradorUnidade = new ClsMoradorUnidade();

        string idAp = "";
        bool lDadosUnidadeOk;
        ClsPessoa lClsPessoa;

        public bool DadosUnidadeOk { get { return lDadosUnidadeOk; } set { lDadosUnidadeOk = value; } }

        public ClsPessoa Pessoa { get { return lClsPessoa; } set { lClsPessoa = value; } }

        public FrmCadastrarMoradorUnidade()
        {
            InitializeComponent();
        }


        private void FrmCadastrarMoradorUnidade_Load(object sender, EventArgs e)
        {
            mskCpf.Text = lClsPessoa.Cpf;
            clsMoradorUnidade.carregarComboBloco(ref cbxBlobo);
            cbxEscada.SelectedIndex = 0;
            clsMoradorUnidade.carregarComboNumAp(ref cbxNumeroApartamento, cbxBlobo.Text, cbxEscada.Text);
        }

        private void btnProcuraAP_Click(object sender, EventArgs e)
        {
            idAp = clsMoradorUnidade.buscarIdAp(cbxBlobo.Text, cbxEscada.Text, cbxNumeroApartamento.Text);
            if (idAp != "")
                txtIdAp.Text = idAp;
            else
                MessageBox.Show("Nenhuma unidade encontrada! =(", "EasyAccess", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (txtIdAp.Text == "")
                MessageBox.Show("Escolha uma unidade para o morador!", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                clsMoradorUnidade.Observavao = txtObservacao.Text;
                clsMoradorUnidade.IdAp = Convert.ToInt16(txtIdAp.Text);

                if (clsMoradorUnidade.gravarMoradorUnidade(lClsPessoa))
                {
                    MessageBox.Show("Morador cadastrado na unidade!", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lDadosUnidadeOk = true;
                }
                else
                {
                    MessageBox.Show("O cadastro não foi feito! =(", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    lDadosUnidadeOk = false;
                }
                this.Close();                
            }
        }

        private void cbxBlobo_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsMoradorUnidade.carregarComboNumAp(ref cbxNumeroApartamento, cbxBlobo.Text, cbxEscada.Text);
        }

        private void cbxEscada_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsMoradorUnidade.carregarComboNumAp(ref cbxNumeroApartamento, cbxBlobo.Text, cbxEscada.Text);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
            
        }
    }
}
