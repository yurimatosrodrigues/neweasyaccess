﻿using easyAccess.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace easyAccess.Views
{
    public partial class FrmConsultarUnidade : Form
    {
        public FrmConsultarUnidade()
        {
            InitializeComponent();
        }

        DataTable dt = new DataTable();
        ClsUnidadesCondominiais clsUnidades = new ClsUnidadesCondominiais();

        private void FrmConsultarUnidade_Load(object sender, EventArgs e)
        {

            clsUnidades.consultarUnidades(ref dt);
            dgvUnidades.DataSource = dt;
        }
    }
}
