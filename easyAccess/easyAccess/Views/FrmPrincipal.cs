﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using easyAccess.Views;


namespace easyAccess.Views
{
    public partial class FrmPrincipal : Form
    {

        private int id;
        private string usuario = "";

        public int Id { get { return id; } set { id = value; } }

        public string Usuario{  get{ return usuario; } set { usuario = value; }  }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            
        }

        public FrmPrincipal()
        {
            InitializeComponent();
        }
        
        private void pessoasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCadastrarPessoa cadastrarPessoa = new FrmCadastrarPessoa();
            cadastrarPessoa.adicionando = true;
            cadastrarPessoa.Show();
        }

        private void acessos_Click(object sender, EventArgs e)
        {
            FrmCadastrarAcesso cadastrarAcesso = new FrmCadastrarAcesso();
            cadastrarAcesso.IdUsuario = id;
            cadastrarAcesso.ShowDialog();
        }
        

        private void pbxRegistrarAcesso_MouseMove(object sender, MouseEventArgs e)
        {
            pbxRegistrarAcesso.BackColor = Color.LightBlue;
            lblRegistrarAcesso.BackColor = Color.LightBlue;

        }

        private void pbxRegistrarAcesso_MouseLeave(object sender, EventArgs e)
        {
            pbxRegistrarAcesso.BackColor = Color.Black;
            lblRegistrarAcesso.BackColor = Color.Black;
        }

        private void pbxRegistrarAcesso_Click(object sender, EventArgs e)
        {

            FrmCadastrarAcesso frmCadastrarAcesso = new FrmCadastrarAcesso();
            frmCadastrarAcesso.IdUsuario = id;                
            frmCadastrarAcesso.ShowDialog();
        }

        private void lblRegistrarAcesso_MouseMove(object sender, MouseEventArgs e)
        {
            pbxRegistrarAcesso.BackColor = Color.LightBlue;
            lblRegistrarAcesso.BackColor = Color.LightBlue;
        }

        private void lblRegistrarAcesso_MouseLeave(object sender, EventArgs e)
        {
            pbxRegistrarAcesso.BackColor = Color.Black;
            lblRegistrarAcesso.BackColor = Color.Black;
        }
       
        private void lblRegistrarAcesso_Click_1(object sender, EventArgs e)
        {
            FrmCadastrarAcesso frmCadastrarAcesso = new FrmCadastrarAcesso();
            frmCadastrarAcesso.Show();
        }

        private void pbxCadastrarPessoa_Click(object sender, EventArgs e)
        {
            FrmCadastrarPessoa frmCadastrarPessoa = new FrmCadastrarPessoa();
            frmCadastrarPessoa.Show();
        }

        private void lblCadastrarPessoa_Click(object sender, EventArgs e)
        {
            FrmCadastrarPessoa frmCadastrarPessoa = new FrmCadastrarPessoa();
            frmCadastrarPessoa.Show();
        }

        private void pbxCadastrarPessoa_MouseMove(object sender, MouseEventArgs e)
        {
            pbxCadastrarPessoa.BackColor = Color.LightBlue;
            lblCadastrarPessoa.BackColor = Color.LightBlue;
        }

        private void pbxCadastrarPessoa_MouseLeave(object sender, EventArgs e)
        {
            pbxCadastrarPessoa.BackColor = Color.Black;
            lblCadastrarPessoa.BackColor = Color.Black;
        }

        private void lblCadastrarPessoa_MouseMove(object sender, MouseEventArgs e)
        {
            pbxCadastrarPessoa.BackColor = Color.LightBlue;
            lblCadastrarPessoa.BackColor = Color.LightBlue;
        }

        private void lblCadastrarPessoa_MouseLeave(object sender, EventArgs e)
        {
            pbxCadastrarPessoa.BackColor = Color.Black;
            lblCadastrarPessoa.BackColor = Color.Black;
        }

                 

        private void moradoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConsultarPessoa frmConsultarPessoa = new FrmConsultarPessoa();
            frmConsultarPessoa.Show();
        }

        private void acessosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConsultarAcessos frmConsultarAcesso = new FrmConsultarAcessos();
            frmConsultarAcesso.Show();
        }

        private void sobreToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmSobre frmsobre = new FrmSobre();
            frmsobre.Show();
        }

        private void usuárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCadastrarUsuario frmCadastrarUsuario = new FrmCadastrarUsuario();
            frmCadastrarUsuario.ShowDialog();
        }

        private void FrmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void sairToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ajudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAjuda frmAjuda = new FrmAjuda();
            frmAjuda.Show();
        }

        private void unidadesCondominiaisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConsultarUnidade frmConsultarUnidade = new FrmConsultarUnidade();
            frmConsultarUnidade.Show();
        }
    }
}
