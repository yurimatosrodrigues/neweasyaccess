﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace easyAccess.Views
{
    public partial class FrmPermissao : Form
    {
        public FrmPermissao()
        {
            InitializeComponent();
        }

        private void FrmPermissao_Load(object sender, EventArgs e)
        {
            pnlPermissao.BackColor = Color.Green;
            lblPermissao1.Text = "Acesso permitido!";
            lblPermissao2.Text = "Yuri Matos Rodrigues";
            lblPermissao3.Text = "Bloco 2, Apartamento 51";
            pbxMorador.Visible = true;
            pnlCentral.BackColor = Color.LightGray;
        }
    }
}
