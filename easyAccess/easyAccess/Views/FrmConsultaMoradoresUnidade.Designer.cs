﻿namespace easyAccess.Views
{
    partial class FrmConsultaMoradoresUnidade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvMoradoresUnidaes = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnConsultarPessoa = new System.Windows.Forms.Button();
            this.gpbFiltros = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbxNumeroApartamento = new System.Windows.Forms.ComboBox();
            this.cbxEscada = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxBloco = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMoradoresUnidaes)).BeginInit();
            this.panel1.SuspendLayout();
            this.gpbFiltros.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMoradoresUnidaes
            // 
            this.dgvMoradoresUnidaes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMoradoresUnidaes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMoradoresUnidaes.Location = new System.Drawing.Point(14, 226);
            this.dgvMoradoresUnidaes.Name = "dgvMoradoresUnidaes";
            this.dgvMoradoresUnidaes.Size = new System.Drawing.Size(669, 255);
            this.dgvMoradoresUnidaes.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(697, 73);
            this.panel1.TabIndex = 29;
            // 
            // btnAlterar
            // 
            this.btnAlterar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar.Location = new System.Drawing.Point(390, 183);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(157, 26);
            this.btnAlterar.TabIndex = 31;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnConsultarPessoa
            // 
            this.btnConsultarPessoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultarPessoa.Location = new System.Drawing.Point(145, 183);
            this.btnConsultarPessoa.Name = "btnConsultarPessoa";
            this.btnConsultarPessoa.Size = new System.Drawing.Size(157, 26);
            this.btnConsultarPessoa.TabIndex = 30;
            this.btnConsultarPessoa.Text = "Consultar";
            this.btnConsultarPessoa.UseVisualStyleBackColor = true;
            // 
            // gpbFiltros
            // 
            this.gpbFiltros.Controls.Add(this.label3);
            this.gpbFiltros.Controls.Add(this.label5);
            this.gpbFiltros.Controls.Add(this.cbxNumeroApartamento);
            this.gpbFiltros.Controls.Add(this.cbxEscada);
            this.gpbFiltros.Controls.Add(this.label2);
            this.gpbFiltros.Controls.Add(this.cbxBloco);
            this.gpbFiltros.Location = new System.Drawing.Point(98, 86);
            this.gpbFiltros.Name = "gpbFiltros";
            this.gpbFiltros.Size = new System.Drawing.Size(496, 86);
            this.gpbFiltros.TabIndex = 32;
            this.gpbFiltros.TabStop = false;
            this.gpbFiltros.Text = "Filtros";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(349, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 18);
            this.label3.TabIndex = 22;
            this.label3.Text = "Nº Apartamento:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(187, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 18);
            this.label5.TabIndex = 23;
            this.label5.Text = "Escada:";
            // 
            // cbxNumeroApartamento
            // 
            this.cbxNumeroApartamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxNumeroApartamento.FormattingEnabled = true;
            this.cbxNumeroApartamento.Location = new System.Drawing.Point(352, 43);
            this.cbxNumeroApartamento.Name = "cbxNumeroApartamento";
            this.cbxNumeroApartamento.Size = new System.Drawing.Size(121, 26);
            this.cbxNumeroApartamento.TabIndex = 5;
            // 
            // cbxEscada
            // 
            this.cbxEscada.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEscada.FormattingEnabled = true;
            this.cbxEscada.Location = new System.Drawing.Point(190, 43);
            this.cbxEscada.Name = "cbxEscada";
            this.cbxEscada.Size = new System.Drawing.Size(121, 26);
            this.cbxEscada.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(27, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 18);
            this.label2.TabIndex = 20;
            this.label2.Text = "Bloco:";
            // 
            // cbxBloco
            // 
            this.cbxBloco.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxBloco.FormattingEnabled = true;
            this.cbxBloco.Location = new System.Drawing.Point(30, 43);
            this.cbxBloco.Name = "cbxBloco";
            this.cbxBloco.Size = new System.Drawing.Size(121, 26);
            this.cbxBloco.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::easyAccess.Properties.Resources.logo6;
            this.pictureBox1.Location = new System.Drawing.Point(14, -41);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(178, 155);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // FrmConsultaMoradoresUnidade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(695, 493);
            this.Controls.Add(this.gpbFiltros);
            this.Controls.Add(this.btnAlterar);
            this.Controls.Add(this.btnConsultarPessoa);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgvMoradoresUnidaes);
            this.Name = "FrmConsultaMoradoresUnidade";
            this.Text = "Consultar Moradores Unidades";
            ((System.ComponentModel.ISupportInitialize)(this.dgvMoradoresUnidaes)).EndInit();
            this.panel1.ResumeLayout(false);
            this.gpbFiltros.ResumeLayout(false);
            this.gpbFiltros.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMoradoresUnidaes;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnConsultarPessoa;
        private System.Windows.Forms.GroupBox gpbFiltros;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbxNumeroApartamento;
        private System.Windows.Forms.ComboBox cbxEscada;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxBloco;
    }
}