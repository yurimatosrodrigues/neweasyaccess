﻿namespace easyAccess.Views
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cadastrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pessoasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acessos = new System.Windows.Forms.ToolStripMenuItem();
            this.usuárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moradoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acessosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.lblRegistrarAcesso = new System.Windows.Forms.Label();
            this.pbxRegistrarAcesso = new System.Windows.Forms.PictureBox();
            this.lblCadastrarPessoa = new System.Windows.Forms.Label();
            this.pbxCadastrarPessoa = new System.Windows.Forms.PictureBox();
            this.unidadesCondominiaisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRegistrarAcesso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCadastrarPessoa)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrosToolStripMenuItem,
            this.consultasToolStripMenuItem,
            this.sobreToolStripMenuItem,
            this.sairToolStripMenuItem,
            this.sairToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1038, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cadastrosToolStripMenuItem
            // 
            this.cadastrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pessoasToolStripMenuItem,
            this.acessos,
            this.usuárioToolStripMenuItem});
            this.cadastrosToolStripMenuItem.Name = "cadastrosToolStripMenuItem";
            this.cadastrosToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.cadastrosToolStripMenuItem.Text = "Cadastros";
            // 
            // pessoasToolStripMenuItem
            // 
            this.pessoasToolStripMenuItem.Name = "pessoasToolStripMenuItem";
            this.pessoasToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.pessoasToolStripMenuItem.Text = "Pessoas";
            this.pessoasToolStripMenuItem.Click += new System.EventHandler(this.pessoasToolStripMenuItem_Click);
            // 
            // acessos
            // 
            this.acessos.Name = "acessos";
            this.acessos.Size = new System.Drawing.Size(116, 22);
            this.acessos.Text = "Acessos";
            this.acessos.Click += new System.EventHandler(this.acessos_Click);
            // 
            // usuárioToolStripMenuItem
            // 
            this.usuárioToolStripMenuItem.Name = "usuárioToolStripMenuItem";
            this.usuárioToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.usuárioToolStripMenuItem.Text = "Usuário";
            this.usuárioToolStripMenuItem.Click += new System.EventHandler(this.usuárioToolStripMenuItem_Click);
            // 
            // consultasToolStripMenuItem
            // 
            this.consultasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moradoresToolStripMenuItem,
            this.acessosToolStripMenuItem,
            this.unidadesCondominiaisToolStripMenuItem});
            this.consultasToolStripMenuItem.Name = "consultasToolStripMenuItem";
            this.consultasToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.consultasToolStripMenuItem.Text = "Consultas";
            // 
            // moradoresToolStripMenuItem
            // 
            this.moradoresToolStripMenuItem.Name = "moradoresToolStripMenuItem";
            this.moradoresToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.moradoresToolStripMenuItem.Text = "Pessoas";
            this.moradoresToolStripMenuItem.Click += new System.EventHandler(this.moradoresToolStripMenuItem_Click);
            // 
            // acessosToolStripMenuItem
            // 
            this.acessosToolStripMenuItem.Name = "acessosToolStripMenuItem";
            this.acessosToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.acessosToolStripMenuItem.Text = "Acessos";
            this.acessosToolStripMenuItem.Click += new System.EventHandler(this.acessosToolStripMenuItem_Click);
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sobreToolStripMenuItem1});
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.sobreToolStripMenuItem.Text = "Sobre";
            // 
            // sobreToolStripMenuItem1
            // 
            this.sobreToolStripMenuItem1.Name = "sobreToolStripMenuItem1";
            this.sobreToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.sobreToolStripMenuItem1.Text = "Sobre";
            this.sobreToolStripMenuItem1.Click += new System.EventHandler(this.sobreToolStripMenuItem1_Click);
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajudaToolStripMenuItem});
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.sairToolStripMenuItem.Text = "Ajuda";
            // 
            // ajudaToolStripMenuItem
            // 
            this.ajudaToolStripMenuItem.Name = "ajudaToolStripMenuItem";
            this.ajudaToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.ajudaToolStripMenuItem.Text = "Ajuda";
            this.ajudaToolStripMenuItem.Click += new System.EventHandler(this.ajudaToolStripMenuItem_Click);
            // 
            // sairToolStripMenuItem1
            // 
            this.sairToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sairToolStripMenuItem2});
            this.sairToolStripMenuItem1.Name = "sairToolStripMenuItem1";
            this.sairToolStripMenuItem1.Size = new System.Drawing.Size(38, 20);
            this.sairToolStripMenuItem1.Text = "Sair";
            // 
            // sairToolStripMenuItem2
            // 
            this.sairToolStripMenuItem2.Name = "sairToolStripMenuItem2";
            this.sairToolStripMenuItem2.Size = new System.Drawing.Size(93, 22);
            this.sairToolStripMenuItem2.Text = "Sair";
            this.sairToolStripMenuItem2.Click += new System.EventHandler(this.sairToolStripMenuItem2_Click);
            // 
            // lblRegistrarAcesso
            // 
            this.lblRegistrarAcesso.AutoSize = true;
            this.lblRegistrarAcesso.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistrarAcesso.ForeColor = System.Drawing.Color.White;
            this.lblRegistrarAcesso.Location = new System.Drawing.Point(853, 615);
            this.lblRegistrarAcesso.Name = "lblRegistrarAcesso";
            this.lblRegistrarAcesso.Size = new System.Drawing.Size(131, 20);
            this.lblRegistrarAcesso.TabIndex = 11;
            this.lblRegistrarAcesso.Text = "Registrar Acesso";
            this.lblRegistrarAcesso.Click += new System.EventHandler(this.lblRegistrarAcesso_Click_1);
            this.lblRegistrarAcesso.MouseLeave += new System.EventHandler(this.lblRegistrarAcesso_MouseLeave);
            this.lblRegistrarAcesso.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lblRegistrarAcesso_MouseMove);
            // 
            // pbxRegistrarAcesso
            // 
            this.pbxRegistrarAcesso.Image = global::easyAccess.Properties.Resources.cadPS;
            this.pbxRegistrarAcesso.Location = new System.Drawing.Point(855, 490);
            this.pbxRegistrarAcesso.Name = "pbxRegistrarAcesso";
            this.pbxRegistrarAcesso.Size = new System.Drawing.Size(126, 117);
            this.pbxRegistrarAcesso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxRegistrarAcesso.TabIndex = 9;
            this.pbxRegistrarAcesso.TabStop = false;
            this.pbxRegistrarAcesso.Click += new System.EventHandler(this.pbxRegistrarAcesso_Click);
            this.pbxRegistrarAcesso.MouseLeave += new System.EventHandler(this.pbxRegistrarAcesso_MouseLeave);
            this.pbxRegistrarAcesso.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbxRegistrarAcesso_MouseMove);
            // 
            // lblCadastrarPessoa
            // 
            this.lblCadastrarPessoa.AutoSize = true;
            this.lblCadastrarPessoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCadastrarPessoa.ForeColor = System.Drawing.Color.White;
            this.lblCadastrarPessoa.Location = new System.Drawing.Point(417, 615);
            this.lblCadastrarPessoa.Name = "lblCadastrarPessoa";
            this.lblCadastrarPessoa.Size = new System.Drawing.Size(136, 20);
            this.lblCadastrarPessoa.TabIndex = 15;
            this.lblCadastrarPessoa.Text = "Cadastrar Pessoa";
            this.lblCadastrarPessoa.Click += new System.EventHandler(this.lblCadastrarPessoa_Click);
            this.lblCadastrarPessoa.MouseLeave += new System.EventHandler(this.lblCadastrarPessoa_MouseLeave);
            this.lblCadastrarPessoa.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lblCadastrarPessoa_MouseMove);
            // 
            // pbxCadastrarPessoa
            // 
            this.pbxCadastrarPessoa.Image = global::easyAccess.Properties.Resources.cadVisitante;
            this.pbxCadastrarPessoa.Location = new System.Drawing.Point(419, 490);
            this.pbxCadastrarPessoa.Name = "pbxCadastrarPessoa";
            this.pbxCadastrarPessoa.Size = new System.Drawing.Size(126, 117);
            this.pbxCadastrarPessoa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCadastrarPessoa.TabIndex = 14;
            this.pbxCadastrarPessoa.TabStop = false;
            this.pbxCadastrarPessoa.Click += new System.EventHandler(this.pessoasToolStripMenuItem_Click);
            this.pbxCadastrarPessoa.MouseLeave += new System.EventHandler(this.pbxCadastrarPessoa_MouseLeave);
            this.pbxCadastrarPessoa.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbxCadastrarPessoa_MouseMove);
            // 
            // unidadesCondominiaisToolStripMenuItem
            // 
            this.unidadesCondominiaisToolStripMenuItem.Name = "unidadesCondominiaisToolStripMenuItem";
            this.unidadesCondominiaisToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.unidadesCondominiaisToolStripMenuItem.Text = "Unidades Condominiais";
            this.unidadesCondominiaisToolStripMenuItem.Click += new System.EventHandler(this.unidadesCondominiaisToolStripMenuItem_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::easyAccess.Properties.Resources.logo6;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1038, 681);
            this.Controls.Add(this.lblCadastrarPessoa);
            this.Controls.Add(this.pbxCadastrarPessoa);
            this.Controls.Add(this.lblRegistrarAcesso);
            this.Controls.Add(this.pbxRegistrarAcesso);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPrincipal_FormClosed);
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRegistrarAcesso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCadastrarPessoa)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pessoasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acessos;
        private System.Windows.Forms.ToolStripMenuItem acessosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moradoresToolStripMenuItem;
        private System.Windows.Forms.PictureBox pbxRegistrarAcesso;
        private System.Windows.Forms.Label lblRegistrarAcesso;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem1;
        private System.Windows.Forms.Label lblCadastrarPessoa;
        private System.Windows.Forms.PictureBox pbxCadastrarPessoa;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem usuárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem unidadesCondominiaisToolStripMenuItem;
    }
}

