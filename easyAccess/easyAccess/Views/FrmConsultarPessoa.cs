﻿using easyAccess.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace easyAccess.Views
{
    public partial class FrmConsultarPessoa : Form
    {
        public FrmConsultarPessoa()
        {
            InitializeComponent();
        }

        ClsPessoa clsPessoa = new ClsPessoa();
        DataTable dt = new DataTable();

        private void FrmConsultarPessoa_Load(object sender, EventArgs e)
        {

            cbxTipoPessoa.SelectedIndex = 0;

            clsPessoa.Tipo_Pessoa_Str = "";
            clsPessoa.consultarPessoa(ref dt);                        
            dgvPessoas.DataSource = dt;
        }


        private void btnConsultarPessoa_Click(object sender, EventArgs e)
        {
            clsPessoa.Tipo_Pessoa_Str = cbxTipoPessoa.Text;
            clsPessoa.consultarPessoa(ref dt);
            dgvPessoas.DataSource = dt;
        }

        
    }
}
