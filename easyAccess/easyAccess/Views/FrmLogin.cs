﻿using easyAccess.Controller;
using easyAccess.Views;
using System;
using System.Windows.Forms;

namespace easyAccess
{
    public partial class FrmLogin : Form
    {

        FrmPrincipal frmPrincipal = new FrmPrincipal();

        public FrmLogin()
        {
            InitializeComponent();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            btnEntrar.Focus();
            btnEntrar.DialogResult = DialogResult.OK;
            btnSair.DialogResult = DialogResult.Cancel;
            this.AcceptButton = btnEntrar;
            this.CancelButton = btnSair;

        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            ClsLogin clsLogin = new ClsLogin();

            clsLogin.Usuario = txtUsuario.Text;
            clsLogin.Senha = txtSenha.Text;

            clsLogin.acessar();

            if (clsLogin.mensagem.Equals(""))
            {
                if (clsLogin.tem)
                {
                                         
                    frmPrincipal.Usuario = txtUsuario.Text;             
                    frmPrincipal.Id = clsLogin.Id;

                    frmPrincipal.Show();

                    this.Hide();
        
                }
                else
                    MessageBox.Show("Usuario ou senha incorretos!", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);                
            }
            else
                MessageBox.Show(clsLogin.mensagem);
        }

            

        private void btnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
       
    }
}
