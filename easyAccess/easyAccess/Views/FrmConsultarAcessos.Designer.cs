﻿namespace easyAccess.Views
{
    partial class FrmConsultarAcessos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvAcessos = new System.Windows.Forms.DataGridView();
            this.btnBuscarAcessos = new System.Windows.Forms.Button();
            this.dtpDataAcesso = new System.Windows.Forms.DateTimePicker();
            this.gpbFiltros = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbxNumeroApartamento = new System.Windows.Forms.ComboBox();
            this.cbxEscada = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxBloco = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxTipoPessoa = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chbDataExata = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcessos)).BeginInit();
            this.gpbFiltros.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAcessos
            // 
            this.dgvAcessos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAcessos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAcessos.Location = new System.Drawing.Point(18, 236);
            this.dgvAcessos.Name = "dgvAcessos";
            this.dgvAcessos.Size = new System.Drawing.Size(1304, 416);
            this.dgvAcessos.TabIndex = 0;
            // 
            // btnBuscarAcessos
            // 
            this.btnBuscarAcessos.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarAcessos.Location = new System.Drawing.Point(18, 197);
            this.btnBuscarAcessos.Name = "btnBuscarAcessos";
            this.btnBuscarAcessos.Size = new System.Drawing.Size(1309, 26);
            this.btnBuscarAcessos.TabIndex = 1;
            this.btnBuscarAcessos.Text = "Buscar";
            this.btnBuscarAcessos.UseVisualStyleBackColor = true;
            this.btnBuscarAcessos.Click += new System.EventHandler(this.btnBuscarAcessos_Click);
            // 
            // dtpDataAcesso
            // 
            this.dtpDataAcesso.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDataAcesso.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataAcesso.Location = new System.Drawing.Point(20, 45);
            this.dtpDataAcesso.MaxDate = new System.DateTime(2018, 11, 28, 0, 0, 0, 0);
            this.dtpDataAcesso.Name = "dtpDataAcesso";
            this.dtpDataAcesso.Size = new System.Drawing.Size(135, 24);
            this.dtpDataAcesso.TabIndex = 0;
            this.dtpDataAcesso.Value = new System.DateTime(2018, 11, 28, 0, 0, 0, 0);
            // 
            // gpbFiltros
            // 
            this.gpbFiltros.Controls.Add(this.chbDataExata);
            this.gpbFiltros.Controls.Add(this.label1);
            this.gpbFiltros.Controls.Add(this.label3);
            this.gpbFiltros.Controls.Add(this.label5);
            this.gpbFiltros.Controls.Add(this.dtpDataAcesso);
            this.gpbFiltros.Controls.Add(this.cbxNumeroApartamento);
            this.gpbFiltros.Controls.Add(this.cbxEscada);
            this.gpbFiltros.Controls.Add(this.label2);
            this.gpbFiltros.Controls.Add(this.cbxBloco);
            this.gpbFiltros.Controls.Add(this.label4);
            this.gpbFiltros.Controls.Add(this.cbxTipoPessoa);
            this.gpbFiltros.Location = new System.Drawing.Point(201, 91);
            this.gpbFiltros.Name = "gpbFiltros";
            this.gpbFiltros.Size = new System.Drawing.Size(943, 95);
            this.gpbFiltros.TabIndex = 27;
            this.gpbFiltros.TabStop = false;
            this.gpbFiltros.Text = "Filtros";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(17, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 18);
            this.label1.TabIndex = 24;
            this.label1.Text = "Data do Acesso:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(790, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 18);
            this.label3.TabIndex = 22;
            this.label3.Text = "Nº Apartamento:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(624, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 18);
            this.label5.TabIndex = 23;
            this.label5.Text = "Escada:";
            // 
            // cbxNumeroApartamento
            // 
            this.cbxNumeroApartamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxNumeroApartamento.FormattingEnabled = true;
            this.cbxNumeroApartamento.Location = new System.Drawing.Point(793, 46);
            this.cbxNumeroApartamento.Name = "cbxNumeroApartamento";
            this.cbxNumeroApartamento.Size = new System.Drawing.Size(121, 26);
            this.cbxNumeroApartamento.TabIndex = 5;
            // 
            // cbxEscada
            // 
            this.cbxEscada.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEscada.FormattingEnabled = true;
            this.cbxEscada.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cbxEscada.Location = new System.Drawing.Point(627, 45);
            this.cbxEscada.Name = "cbxEscada";
            this.cbxEscada.Size = new System.Drawing.Size(121, 26);
            this.cbxEscada.TabIndex = 4;
            this.cbxEscada.SelectedIndexChanged += new System.EventHandler(this.cbxEscada_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(465, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 18);
            this.label2.TabIndex = 20;
            this.label2.Text = "Bloco:";
            // 
            // cbxBloco
            // 
            this.cbxBloco.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxBloco.FormattingEnabled = true;
            this.cbxBloco.Location = new System.Drawing.Point(468, 45);
            this.cbxBloco.Name = "cbxBloco";
            this.cbxBloco.Size = new System.Drawing.Size(121, 26);
            this.cbxBloco.TabIndex = 3;
            this.cbxBloco.SelectedIndexChanged += new System.EventHandler(this.cbxBloco_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(280, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "Tipo Pessoa:";
            // 
            // cbxTipoPessoa
            // 
            this.cbxTipoPessoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxTipoPessoa.FormattingEnabled = true;
            this.cbxTipoPessoa.Items.AddRange(new object[] {
            "Morador",
            "Visitante",
            "Prestador de Serviço",
            "Todos"});
            this.cbxTipoPessoa.Location = new System.Drawing.Point(283, 45);
            this.cbxTipoPessoa.Name = "cbxTipoPessoa";
            this.cbxTipoPessoa.Size = new System.Drawing.Size(147, 26);
            this.cbxTipoPessoa.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1410, 73);
            this.panel2.TabIndex = 28;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::easyAccess.Properties.Resources.logo6;
            this.pictureBox1.Location = new System.Drawing.Point(18, -41);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(168, 151);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // chbDataExata
            // 
            this.chbDataExata.AutoSize = true;
            this.chbDataExata.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbDataExata.Location = new System.Drawing.Point(168, 47);
            this.chbDataExata.Name = "chbDataExata";
            this.chbDataExata.Size = new System.Drawing.Size(99, 22);
            this.chbDataExata.TabIndex = 29;
            this.chbDataExata.Text = "Data Exata";
            this.chbDataExata.UseVisualStyleBackColor = true;
            // 
            // FrmConsultarAcessos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(1339, 664);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.gpbFiltros);
            this.Controls.Add(this.btnBuscarAcessos);
            this.Controls.Add(this.dgvAcessos);
            this.Name = "FrmConsultarAcessos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultar Acessos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmConsultarAcessos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcessos)).EndInit();
            this.gpbFiltros.ResumeLayout(false);
            this.gpbFiltros.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAcessos;
        private System.Windows.Forms.Button btnBuscarAcessos;
        private System.Windows.Forms.DateTimePicker dtpDataAcesso;
        private System.Windows.Forms.GroupBox gpbFiltros;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbxTipoPessoa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxBloco;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbxNumeroApartamento;
        private System.Windows.Forms.ComboBox cbxEscada;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbDataExata;
    }
}