﻿using easyAccess.Controller;
using System;
using System.Windows.Forms;

namespace easyAccess.Views
{
    public partial class FrmCadastrarAcesso : Form
    {
        public FrmCadastrarAcesso()
        {
            InitializeComponent();
        }

        ClsAcesso clsAcesso = new ClsAcesso();
        private int lIdUsuario;

        public int IdUsuario { get { return lIdUsuario;  } set { lIdUsuario = value; } }

        private void FrmCadastrarAcesso_Load(object sender, EventArgs e)
        {
            
            clsAcesso.IdUsuario = lIdUsuario;

            mskCpf.Focus();
            mskCpf.Select();
            mskCpf.Text = "";
            
            groupBox1.Text = "Informações Morador";
            groupBox2.Text = "Informações Morador";
            btnBuscar.Text = "Buscar Morador";
            label8.Text = "CPF";
            mskCpf.Mask = "999,999,999-99";
            txtCpfCnpj.Visible = false;


            cbxBloco.Enabled = false;
            cbxEscada.Enabled = false;
            cbxNumeroApartamento.Enabled = false;
            cbxNome.Enabled = false;

        }

        
        private void rdbMorador_CheckedChanged(object sender, EventArgs e)
        {
            clsAcesso.IdUnidade = -1;

            groupBox1.Text = "Informações Morador";
            groupBox2.Text = "Informações Morador";
            btnBuscar.Text = "Buscar Morador";
            label8.Text = "CPF";
            mskCpf.Mask = "999,999,999-99";
            txtCpfCnpj.Visible = false;
            
            cbxBloco.DataSource = null;
            cbxBloco.Items.Clear();
            cbxEscada.DataSource = null;
            cbxEscada.Items.Clear();
            cbxNumeroApartamento.DataSource = null;
            cbxNumeroApartamento.Items.Clear();
            cbxNome.DataSource = null;
            cbxNome.Items.Clear();

            cbxBloco.Enabled = false;
            cbxEscada.Enabled = false;
            cbxNumeroApartamento.Enabled = false;
            cbxNome.Enabled = false;

            cbxBloco.DropDownStyle = ComboBoxStyle.DropDown;
            cbxEscada.DropDownStyle = ComboBoxStyle.DropDown;
            cbxNumeroApartamento.DropDownStyle = ComboBoxStyle.DropDown;
            cbxNome.DropDownStyle = ComboBoxStyle.DropDown;

            limpaCampos(true);
            
        }

        private void rdbVisitante_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Text = "Informações Visitante";
            groupBox2.Text = "Informações Morador";
            btnBuscar.Text = "Buscar Visitante";
            label8.Text = "CPF";
            mskCpf.Mask = "999,999,999-99";
            txtCpfCnpj.Visible = false;

            rdbVisitanteEPS();          
        }


        
        private void rdbPrestadorServico_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Text = "Informações Prestador de Serviço";
            groupBox2.Text = "Informações Morador";
            btnBuscar.Text = "Buscar P.S.";
            label8.Text = "CPF/CNPJ";
            mskCpf.Mask = "";
            txtCpfCnpj.Visible = true;

            rdbVisitanteEPS();
        }

        private void rdbVisitanteEPS()
        {

            clsAcesso.carregarComboBloco(ref cbxBloco);

            cbxEscada.Items.Clear();
            cbxEscada.Items.Add("1");
            cbxEscada.Items.Add("2");
            cbxEscada.Items.Add("3");
            cbxEscada.SelectedIndex = 0;
            
            clsAcesso.carregarComboNumAp(ref cbxNumeroApartamento, cbxBloco.Text, cbxEscada.Text);


            cbxBloco.Enabled = true;
            cbxEscada.Enabled = true;
            cbxNumeroApartamento.Enabled = true;
            cbxNome.Enabled = true;

            cbxBloco.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxEscada.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxNumeroApartamento.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxNome.DropDownStyle = ComboBoxStyle.DropDownList;

            clsAcesso.pegaIdUnidade(cbxBloco.Text, cbxEscada.Text, cbxNumeroApartamento.Text);

            limpaCampos(false);

        }

        private void mskCpf_TextChanged(object sender, EventArgs e)
        {
            string LStrCpf = "";
            LStrCpf = mskCpf.Text;

            if (LStrCpf.Length == 11) {
                if (rdbMorador.Checked)
                {

                    if (mskCpf.Text == "" && txtRG.Text == "")
                        MessageBox.Show("Informe CPF ou RG, por favor. =)", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                    {

                        clsAcesso.Cpf = mskCpf.Text;
                        clsAcesso.Rg = txtRG.Text;

                        if (clsAcesso.consultarMorador())
                        {
                            clsAcesso.Cpf = mskCpf.Text;
                            clsAcesso.Rg = txtRG.Text;

                            txtNome.Text = clsAcesso.Nome;
                            txtTelefone.Text = clsAcesso.Telefone;
                            txtRG.Text = clsAcesso.Rg;

                            cbxBloco.Text = clsAcesso.Bloco2;
                            cbxEscada.Text = clsAcesso.Escada2;
                            cbxNumeroApartamento.Text = clsAcesso.NumeroAp2;
                            cbxNome.Text = clsAcesso.Nome3;
                        }
                    }
                }

                else if (rdbVisitante.Checked)
                {

                    if (mskCpf.Text == "" && txtRG.Text == "")
                        MessageBox.Show("Informe CPF ou RG, por favor. =)", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                    {
                        clsAcesso.Cpf = mskCpf.Text;
                        clsAcesso.Rg = txtRG.Text;

                        if (clsAcesso.consultarVisitante())
                        {
                            mskCpf.Text = clsAcesso.Cpf;
                            txtRG.Text = clsAcesso.Rg;
                            txtNome.Text = clsAcesso.Nome;
                            txtTelefone.Text = clsAcesso.Telefone;
                        }
                    }
                }
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (rdbMorador.Checked)
            {

                if (mskCpf.Text == "" && txtRG.Text == "")
                    MessageBox.Show("Informe CPF ou RG, por favor. =)", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {

                    clsAcesso.Cpf = mskCpf.Text;
                    clsAcesso.Rg = txtRG.Text;

                    if (clsAcesso.consultarMorador())
                    {
                        clsAcesso.Cpf = mskCpf.Text;
                        clsAcesso.Rg = txtRG.Text;

                        txtNome.Text = clsAcesso.Nome;
                        txtTelefone.Text = clsAcesso.Telefone;
                        txtRG.Text = clsAcesso.Rg;

                        cbxBloco.Text = clsAcesso.Bloco2;
                        cbxEscada.Text = clsAcesso.Escada2;
                        cbxNumeroApartamento.Text = clsAcesso.NumeroAp2;
                        cbxNome.Text = clsAcesso.Nome3;
                    }
                }
            }

            else if (rdbVisitante.Checked)
            {

                if (mskCpf.Text == "" && txtRG.Text == "")
                    MessageBox.Show("Informe CPF ou RG, por favor. =)", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    clsAcesso.Cpf = mskCpf.Text;
                    clsAcesso.Rg = txtRG.Text;

                    if (clsAcesso.consultarVisitante())
                    {
                        mskCpf.Text = clsAcesso.Cpf;
                        txtRG.Text = clsAcesso.Rg;
                        txtNome.Text = clsAcesso.Nome;
                        txtTelefone.Text = clsAcesso.Telefone;
                    }
                }
            }

            else {
                if(txtCpfCnpj.Text == "" && txtRG.Text == "")
                    MessageBox.Show("Informe CPF, CNPJ ou RG, por favor. =)", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);                    
                else
                {
                    clsAcesso.CpfCnpj= txtCpfCnpj.Text;
                    clsAcesso.Cpf = txtCpfCnpj.Text;
                    clsAcesso.Rg = txtRG.Text;
                    

                    if (clsAcesso.consultarPS())
                    {
                        txtCpfCnpj.Text = clsAcesso.CpfCnpj;
                        txtRG.Text = clsAcesso.Rg;
                        txtNome.Text = clsAcesso.Nome;
                        txtTelefone.Text = clsAcesso.Telefone;
                    }
                }
            }
        }


        private void limpaCampos(bool morador) {

            mskCpf.Text = "";
            txtCpfCnpj.Text = "";
            txtRG.Text = "";
            txtNome.Text = "";
            txtTelefone.Text = "";

            if (morador)
            {

                cbxBloco.Text = "";
                cbxEscada.Text = "";
                cbxNumeroApartamento.Text = "";
                cbxNome.Text = "";

            }
            
            txtObservacoes.Text = "";
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            if (rdbMorador.Checked) limpaCampos(true); else limpaCampos(false);
        }       
        

        private void btnRegistrarAcesso_Click(object sender, EventArgs e)
        {
            clsAcesso.Observacao = txtObservacoes.Text;

            if(validaDados())
                if(clsAcesso.registrarAcesso())
                    limpaCampos(true);
        }

        private void cbxBloco_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsAcesso.carregarComboNumAp(ref cbxNumeroApartamento, cbxBloco.Text, cbxEscada.Text);
            
        }

        private void cbxEscada_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsAcesso.carregarComboNumAp(ref cbxNumeroApartamento, cbxBloco.Text, cbxEscada.Text);
            
        }

        private void cbxNumeroApartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsAcesso.pegaIdUnidade(cbxBloco.Text, cbxEscada.Text, cbxNumeroApartamento.Text);
        }

        private bool validaDados()
        {
            int contaErros = 0;
            string mensagem = "";

            if (txtNome.Text == "")
            {
                contaErros = contaErros + 1;
                mensagem = mensagem + contaErros + " - " + "O campo Nome não foi preenchido!\n";
            }

            if (txtTelefone.Text == "")
            {
                contaErros = contaErros + 1;
                mensagem = mensagem + contaErros + " - " + "O campo Telefone não foi preenchido!\n";
            }

            if (cbxNumeroApartamento.Text == "")
            {
                contaErros = contaErros + 1;
                mensagem = mensagem + contaErros + " - " + "O campo Número do Apartamento não foi preenchido!\n";
            }

            if (contaErros > 0)
            {
                MessageBox.Show(mensagem, "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else
                return true;

        }

       
    }
}
