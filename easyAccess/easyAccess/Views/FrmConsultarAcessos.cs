﻿using easyAccess.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace easyAccess.Views
{
    public partial class FrmConsultarAcessos : Form
    {
        public FrmConsultarAcessos()
        {
            InitializeComponent();
        }

        ClsMoradorUnidade clsMoradorUnidade = new ClsMoradorUnidade();

        ClsAcesso clsAcesso = new ClsAcesso();
        DataTable dt = new DataTable();

        private void FrmConsultarAcessos_Load(object sender, EventArgs e)
        {
            clsAcesso.consultarAcessos(ref dt);
            dgvAcessos.DataSource = dt;

            clsMoradorUnidade.carregarComboBloco(ref cbxBloco);
            cbxEscada.SelectedIndex = 0;
            clsMoradorUnidade.carregarComboNumAp(ref cbxNumeroApartamento, cbxBloco.Text, cbxEscada.Text);
            cbxBloco.Text = "";
            cbxEscada.Text = "";
            cbxNumeroApartamento.Text = "";
        }

        private void btnBuscarAcessos_Click(object sender, EventArgs e)
        {

            string bloco, escada, numap;
            string data = dtpDataAcesso.Text;

            string dataExata = "N";

            if (chbDataExata.Checked)
                dataExata = "S";


            bloco = cbxBloco.Text;
            escada = cbxEscada.Text;
            numap = cbxNumeroApartamento.Text;
            
            if (!(cbxBloco.Text != "" && cbxEscada.Text != "" && cbxNumeroApartamento.Text != ""))
                bloco = escada = numap = "";


            clsAcesso.consultarAcessos(ref dt, Convert.ToDateTime(data), cbxTipoPessoa.Text, dataExata, bloco, escada, numap);
            dgvAcessos.DataSource = dt;
        }

        private void cbxBloco_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsMoradorUnidade.carregarComboNumAp(ref cbxNumeroApartamento, cbxBloco.Text, cbxEscada.Text);
        }

        private void cbxEscada_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsMoradorUnidade.carregarComboNumAp(ref cbxNumeroApartamento, cbxBloco.Text, cbxEscada.Text);
        }
    }
}
