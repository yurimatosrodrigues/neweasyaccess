﻿namespace easyAccess.Views
{
    partial class FrmPermissao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCentral = new System.Windows.Forms.Panel();
            this.lblPermissao3 = new System.Windows.Forms.Label();
            this.lblPermissao2 = new System.Windows.Forms.Label();
            this.pnlPermissao = new System.Windows.Forms.Panel();
            this.pbxMorador = new System.Windows.Forms.PictureBox();
            this.lblPermissao1 = new System.Windows.Forms.Label();
            this.pnlCentral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMorador)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCentral
            // 
            this.pnlCentral.BackColor = System.Drawing.Color.LightGray;
            this.pnlCentral.Controls.Add(this.lblPermissao3);
            this.pnlCentral.Controls.Add(this.lblPermissao2);
            this.pnlCentral.Controls.Add(this.pnlPermissao);
            this.pnlCentral.Controls.Add(this.pbxMorador);
            this.pnlCentral.Controls.Add(this.lblPermissao1);
            this.pnlCentral.Location = new System.Drawing.Point(-1, -1);
            this.pnlCentral.Name = "pnlCentral";
            this.pnlCentral.Size = new System.Drawing.Size(417, 267);
            this.pnlCentral.TabIndex = 7;
            // 
            // lblPermissao3
            // 
            this.lblPermissao3.AutoSize = true;
            this.lblPermissao3.Font = new System.Drawing.Font("Segoe Print", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPermissao3.ForeColor = System.Drawing.Color.Black;
            this.lblPermissao3.Location = new System.Drawing.Point(84, 219);
            this.lblPermissao3.Name = "lblPermissao3";
            this.lblPermissao3.Size = new System.Drawing.Size(0, 33);
            this.lblPermissao3.TabIndex = 8;
            // 
            // lblPermissao2
            // 
            this.lblPermissao2.AutoSize = true;
            this.lblPermissao2.Font = new System.Drawing.Font("Segoe Print", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPermissao2.ForeColor = System.Drawing.Color.Black;
            this.lblPermissao2.Location = new System.Drawing.Point(106, 188);
            this.lblPermissao2.Name = "lblPermissao2";
            this.lblPermissao2.Size = new System.Drawing.Size(0, 33);
            this.lblPermissao2.TabIndex = 7;
            // 
            // pnlPermissao
            // 
            this.pnlPermissao.BackColor = System.Drawing.Color.LightGray;
            this.pnlPermissao.Location = new System.Drawing.Point(194, 117);
            this.pnlPermissao.Name = "pnlPermissao";
            this.pnlPermissao.Size = new System.Drawing.Size(28, 24);
            this.pnlPermissao.TabIndex = 6;
            // 
            // pbxMorador
            // 
            this.pbxMorador.Image = global::easyAccess.Properties.Resources.person_male;
            this.pbxMorador.Location = new System.Drawing.Point(145, 7);
            this.pbxMorador.Name = "pbxMorador";
            this.pbxMorador.Size = new System.Drawing.Size(127, 102);
            this.pbxMorador.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMorador.TabIndex = 5;
            this.pbxMorador.TabStop = false;
            // 
            // lblPermissao1
            // 
            this.lblPermissao1.AutoSize = true;
            this.lblPermissao1.Font = new System.Drawing.Font("Segoe Print", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPermissao1.ForeColor = System.Drawing.Color.Black;
            this.lblPermissao1.Location = new System.Drawing.Point(125, 155);
            this.lblPermissao1.Name = "lblPermissao1";
            this.lblPermissao1.Size = new System.Drawing.Size(0, 33);
            this.lblPermissao1.TabIndex = 0;
            // 
            // FrmPermissao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 264);
            this.Controls.Add(this.pnlCentral);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPermissao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmPermissao";
            this.Load += new System.EventHandler(this.FrmPermissao_Load);
            this.pnlCentral.ResumeLayout(false);
            this.pnlCentral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMorador)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCentral;
        private System.Windows.Forms.Label lblPermissao3;
        private System.Windows.Forms.Label lblPermissao2;
        private System.Windows.Forms.Panel pnlPermissao;
        private System.Windows.Forms.PictureBox pbxMorador;
        private System.Windows.Forms.Label lblPermissao1;
    }
}