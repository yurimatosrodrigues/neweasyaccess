﻿using easyAccess.Controller;
using System.Windows.Forms;

namespace easyAccess
{
    public partial class FrmCadastrarPessoa : Form
    {

        public bool adicionando { get; set; }

        ClsPessoa clsPessoa = new ClsPessoa();
        ClsMoradorUnidade clsMoradorUnidade = new ClsMoradorUnidade();
        
        public FrmCadastrarPessoa()
        {
            InitializeComponent();
        }

        private void FrmCadastrarPessoa_Load(object sender, System.EventArgs e)
        {            
            cbxTipoPessoa.SelectedIndex = 0;
        }

        private void btnSalvar_Click(object sender, System.EventArgs e)
        {

            if (validaDados())
            {

                clsPessoa.Tipo_Pessoa_Str = cbxTipoPessoa.Text;
                clsPessoa.Nome = txtNome.Text;
                clsPessoa.Rg = txtRg.Text;
                clsPessoa.Cpf = mskCpf.Text;
                clsPessoa.Cnpj = txtCpfCnpj.Text;
                clsPessoa.Ddd = mskDdd.Text;
                clsPessoa.Telefone = txtTelefone.Text;
                clsPessoa.Email = txtEmail.Text;


                if (adicionando && cbxTipoPessoa.Text == "Morador")
                {
                    FrmCadastrarMoradorUnidade frmCadastrarMoradorUnidade = new FrmCadastrarMoradorUnidade();
                    frmCadastrarMoradorUnidade.Pessoa = clsPessoa;
                    frmCadastrarMoradorUnidade.ShowDialog();
                    
                    if (frmCadastrarMoradorUnidade.DadosUnidadeOk)
                        limpa_campos();                    
                }
                else {
                    if (clsPessoa.gravarPessoa(adicionando))
                    {
                        MessageBox.Show("Cadastro realizado com sucesso!", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        limpa_campos();
                    }
                }                
            }            
        }

        private bool validaDados()
        {
            int contaErros = 0;
            string mensagem = "";
            

            if (txtNome.Text == "") {
                contaErros = contaErros + 1;
                mensagem = mensagem + contaErros + " - " + "O campo Nome não foi preenchido!\n";                                
            }

            if (txtTelefone.Text == "")
            {
                contaErros = contaErros + 1;
                mensagem = mensagem + contaErros + " - " + "O campo Telefone não foi preenchido!\n";
            }
            
            if (txtRg.Text == "")
            {
                contaErros = contaErros + 1;
                mensagem = mensagem + contaErros + " - " + "O campo RG não foi preenchido!\n";
            }


            if(cbxTipoPessoa.Text == "Prestador de Serviço")
            {
                if (txtCpfCnpj.Text == "") {
                    contaErros = contaErros + 1;
                    mensagem = mensagem + contaErros + " - " + "O campo CPF/CNPJ não foi preenchido!\n";
                }
            }


            if (cbxTipoPessoa.Text == "") {
                contaErros = contaErros + 1;
                mensagem = mensagem + contaErros + " - " + "O campo Tipo Pessoa não foi preenchido!\n";
            }

            if (cbxTipoPessoa.Text == "Morador" || cbxTipoPessoa.Text == "Visitante") {
                if (mskCpf.Text == "")
                {
                    contaErros = contaErros + 1;
                    mensagem = mensagem + contaErros + " - " + "O campo CPF não foi preenchido!\n";
                }
                                
            }

            if (txtEmail.Text != "")
            {
                if (!txtEmail.Text.Contains("@")) {
                    contaErros = contaErros + 1;
                    mensagem = mensagem + contaErros + " - " + "O Campo e-mail não foi preenchido corretamente!\n";
                }
            }
                       
            if (contaErros > 0){
                MessageBox.Show(mensagem, "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            else         
                return true;

        }

        private void cbxTipoPessoa_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            if (cbxTipoPessoa.Text == "Prestador de Serviço")
            {
                lblCpf.Text = "CPF/CNPJ";
                txtCpfCnpj.Visible = true;
                txtCpfCnpj.Text = "";
            }
            else {
                lblCpf.Text = "CPF";
                txtCpfCnpj.Visible = false;
            }

        }

        private void btnLimpar_Click(object sender, System.EventArgs e)
        {
            limpa_campos();
        }

        public void limpa_campos() {
            cbxTipoPessoa.Text = "";
            txtCpfCnpj.Text = "";
            mskCpf.Text = "";
            txtEmail.Text = "";
            txtNome.Text = "";
            txtRg.Text = "";
            txtTelefone.Text = "";
            mskDdd.Text = "";
        }

        private void btnCadastrarBiometria_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("Tela em construção... ;)", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
