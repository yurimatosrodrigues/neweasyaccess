﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace easyAccess.Model
{
    public class ClsComandosAcesso
    {


        ClsDb clsDb = new ClsDb();
        string comandos = "";

        public void consultarMorador(string cpf, string rg, ref DataTable dt) {
            try
            {
                comandos = "SELECT 	PES.ID, PES.NOME, PES.CPF, PES.RG, PES.TELEFONE, UC.PREDIO, UC.ESCADA, UC.NUMEROAPARTAMENTO, UC.ID";
                comandos = comandos + " FROM MORADORUNIDADECONDOMINIAL MUC INNER JOIN PESSOA PES";
                comandos = comandos + " ON MUC.PESSOA_ID = PES.ID INNER JOIN  UNIDADECONDOMINIAL UC";
                comandos = comandos + " ON MUC.UNIDADECONDOMINIAL_ID = UC.ID";
                comandos = comandos + " WHERE PES.CPF = @Cpf OR PES.RG = @Rg;";
                // AND TIPOPESSOA = '1'; -> PENSAR SE O MÉTODO DEVE RECEBER PARAMETRO PARA TIPO PESSOA,

                MySqlCommand cmd = new MySqlCommand(comandos);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@Cpf", cpf);
                cmd.Parameters.AddWithValue("@Rg", rg);

                dt = clsDb.retorna_DataTable(cmd);
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        public bool registrarAcesso(string obs, int id_pessoa, int id_usuario, int id_unidade) {

            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = " INSERT INTO ACESSO(DataHora, Observacoes, IdPessoa, Usuario_Id, UnidadeCondominial_Id) VALUES (NOW(), @Observacao, @IdPessoa, @IdUsuario, @IdUnidade);";

            cmd.Parameters.AddWithValue("@Observacao", obs);
            cmd.Parameters.AddWithValue("@IdPessoa", id_pessoa);
            cmd.Parameters.AddWithValue("@IdUsuario", id_usuario);
            cmd.Parameters.AddWithValue("@IdUnidade", id_unidade);
            
            if (clsDb.insereRegistros(cmd))
                return true;
            else
                return false;

        }


        public void comandosCarregarCbxBloco(ref ComboBox cbx)
        {
            comandos = "SELECT PREDIO FROM MORADORUNIDADECONDOMINIAL MUC";
            comandos = comandos + " INNER JOIN UNIDADECONDOMINIAL UC ON MUC.UNIDADECONDOMINIAL_ID = UC.ID";
            comandos = comandos + " GROUP BY PREDIO;";

            string dm = "PREDIO";            

            clsDb.FCarregar_Cbx(ref cbx, comandos, dm);
        }
        

        public void comandosCarregarCbxNumAp(ref ComboBox cbxNumAp, string predio, string escada)
        {

            comandos = "SELECT NUMEROAPARTAMENTO FROM MORADORUNIDADECONDOMINIAL MUC";
            comandos = comandos + " INNER JOIN UNIDADECONDOMINIAL UC ON MUC.UNIDADECONDOMINIAL_ID = UC.ID";
            comandos = comandos + " WHERE UC.PREDIO = '" + predio + "' AND UC.ESCADA = '" + escada + "'";
            comandos = comandos + " GROUP BY NUMEROAPARTAMENTO";       
                  
            string dm = "NUMEROAPARTAMENTO";
            clsDb.FCarregar_Cbx(ref cbxNumAp, comandos, dm);
        }

        public void pegaIdUnidade(ref DataTable dtIdUnid, string bloco, string escada, string numeroap)
        {
            

            comandos = "SELECT UC.ID FROM UNIDADECONDOMINIAL UC INNER JOIN MORADORUNIDADECONDOMINIAL MUC";
            comandos = comandos + " ON UC.ID = MUC.UNIDADECONDOMINIAL_ID WHERE UC.PREDIO = @Bloco AND ESCADA = @Escada AND NUMEROAPARTAMENTO = @NumAP";
            comandos = comandos + " GROUP BY MUC.UNIDADECONDOMINIAL_ID;";


            MySqlCommand cmd = new MySqlCommand(comandos);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Bloco", bloco);
            cmd.Parameters.AddWithValue("@Escada", escada);
            cmd.Parameters.AddWithValue("@NumAp", numeroap);

            dtIdUnid = clsDb.retorna_DataTable(cmd);            
        }
        public bool consultarAcessos(ref DataTable dtAcessos)
        {

            try
            {
                comandos = "SELECT  A.DATAHORA AS 'Data e Hora',";
                comandos = comandos + " CASE WHEN P.TIPOPESSOA = '1' THEN 'Morador' WHEN P.TIPOPESSOA = '2' THEN 'Visitante' WHEN P.TIPOPESSOA = '3' THEN 'Prestador de Serviço' ELSE '' END AS 'Tipo Pessoa',";
                comandos = comandos + " P.NOME AS 'Nome', P.RG, P.CPF, P.CNPJ, P.DDD, P.TELEFONE, P.EMAIL,";
                comandos = comandos + " UC.PREDIO AS 'Bloco', UC.ESCADA As 'Escada', UC.NUMEROAPARTAMENTO AS 'Numero AP', A.OBSERVACOES AS 'Obs. Acesso'";
                comandos = comandos + " FROM ACESSO A INNER JOIN PESSOA P ON A.IDPESSOA = P.ID";
                comandos = comandos + " INNER JOIN UNIDADECONDOMINIAL UC ON A.UNIDADECONDOMINIAL_ID = UC.ID";
                comandos = comandos + " ORDER BY A.DataHora;";


                MySqlCommand cmd = new MySqlCommand(comandos);
                cmd.CommandType = CommandType.Text;

                dtAcessos = clsDb.retorna_DataTable(cmd);
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro na consulta! =( - " + ex.Message, "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }            
        }


        public bool consultarAcessos(ref DataTable dtAcessos, DateTime data, string tipoPessoa, string dataExata, string bloco, string escada, string numap)
        {

            try
            {
                DateTime diaSeguinte = data.AddDays(1);

                comandos = "SELECT  A.DATAHORA AS 'Data e Hora',";
                comandos = comandos + " CASE WHEN P.TIPOPESSOA = '1' THEN 'Morador' WHEN P.TIPOPESSOA = '2' THEN 'Visitante' WHEN P.TIPOPESSOA = '3' THEN 'Prestador de Serviço' ELSE '' END AS 'Tipo Pessoa',";
                comandos = comandos + " P.NOME AS 'Nome', P.RG, P.CPF, P.CNPJ, P.DDD, P.TELEFONE, P.EMAIL,";
                comandos = comandos + " UC.PREDIO AS 'Bloco', UC.ESCADA As 'Escada', UC.NUMEROAPARTAMENTO AS 'Numero AP', A.OBSERVACOES AS 'Obs. Acesso'";
                comandos = comandos + " FROM ACESSO A INNER JOIN PESSOA P ON A.IDPESSOA = P.ID";
                comandos = comandos + " INNER JOIN UNIDADECONDOMINIAL UC ON A.UNIDADECONDOMINIAL_ID = UC.ID";

                if (dataExata == "S")
                    comandos = comandos + " WHERE A.DATAHORA >= @Data AND A.DATAHORA <= @DiaSeguinte";
                else
                    comandos = comandos + " WHERE A.DATAHORA >= @Data";
                
                if (tipoPessoa != "")
                    comandos = comandos + " AND P.TIPOPESSOA = @TipoPessoa";


                if (bloco != "" && escada != "" && numap != "")
                    comandos = comandos + " AND UC.PREDIO = @Bloco AND UC.ESCADA = @Escada AND UC.NUMEROAPARTAMENTO = @NumAp";

                comandos = comandos + " ORDER BY A.DataHora;";

                MySqlCommand cmd = new MySqlCommand(comandos);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@Data", data);
                cmd.Parameters.AddWithValue("@DiaSeguinte", diaSeguinte);
                cmd.Parameters.AddWithValue("@TipoPessoa", tipoPessoa);
                cmd.Parameters.AddWithValue("@Bloco", bloco);
                cmd.Parameters.AddWithValue("@Escada", escada);
                cmd.Parameters.AddWithValue("@NumAp", numap);

                dtAcessos = clsDb.retorna_DataTable(cmd);
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro na consulta! =( - " + ex.Message, "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }


    }
      
   
}
