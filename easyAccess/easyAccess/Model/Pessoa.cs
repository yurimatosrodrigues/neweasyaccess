﻿using System;
using System.Collections.Generic;

namespace easyAccess.Models
{
    public partial class Pessoa
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public string Rg { get; set; }
        public string Telefone { get; set; }
        public string BiometriaPolegar { get; set; }
        public string BiometriaIndicador { get; set; }
        public string BiometriaMedio { get; set; }
        public string TipoPessoa { get; set; }
    }
}
