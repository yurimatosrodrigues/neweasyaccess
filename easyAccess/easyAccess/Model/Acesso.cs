﻿using System;
using System.Collections.Generic;

namespace easyAccess.Models
{
    public partial class Acesso
    {
        public long Id { get; set; }
        public DateTime DataHora { get; set; }
        public string Observacoes { get; set; }
        public long PessoaId { get; set; }
        public long UsuarioId { get; set; }
        public long UnidadeCondominialId { get; set; }
    }
}
