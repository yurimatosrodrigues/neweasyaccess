﻿using System;
using System.Collections.Generic;

namespace easyAccess.Models
{
    public partial class Usuario
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}
