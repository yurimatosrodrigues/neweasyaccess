﻿using System;
using MySql.Data.MySqlClient;
using System.Data;

namespace easyAccess.Model
{
    public class ClsComandosLogin
    {

        public bool tem;
        public String mensagem = "";
        MySqlCommand cmd = new MySqlCommand();
        ClsDb clsDb = new ClsDb();
        MySqlDataReader dr;
        
        DataTable dt = new DataTable();

        public bool verificarLogin(ref int id, String login, String senha) {
            //comandos sql para verificar no banco
                        
            cmd.CommandText = "SELECT * FROM USUARIO WHERE EMAIL = @LOGIN AND SENHA = @SENHA";
            cmd.Parameters.AddWithValue("@LOGIN", login);
            cmd.Parameters.AddWithValue("@SENHA", senha);

            try
            {
                cmd.Connection = clsDb.conectar();
                dr = cmd.ExecuteReader();                
                if (dr.HasRows) {

                    dr.Close();

                    dt = clsDb.retorna_DataTable(cmd);
                    id = Convert.ToInt16(dt.Rows[0][0].ToString());

                    tem = true;
                }                
            }
            catch (MySqlException)
            {                
                this.mensagem = "Erro com o banco de dados";
            }

            return tem;
        }

    }
}
