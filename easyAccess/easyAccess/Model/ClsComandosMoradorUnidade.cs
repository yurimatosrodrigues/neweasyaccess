﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;

namespace easyAccess.Model
{
    public class ClsComandosMoradorUnidade
    {

        ClsDb clsDb = new ClsDb();
        string comando = "";
        public string comandosConsultarIdAp(string bloco, string escada, string numeroAp)
        {
            try
            {
                comando = "SELECT ID FROM UNIDADECONDOMINIAL WHERE PREDIO = @BLOCO AND ESCADA = @ESCADA AND NUMEROAPARTAMENTO = @NUMEROAPARTAMENTO";

                MySqlCommand cmd = new MySqlCommand(comando);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@BLOCO", bloco);
                cmd.Parameters.AddWithValue("@ESCADA", escada);
                cmd.Parameters.AddWithValue("@NUMEROAPARTAMENTO", numeroAp);

                DataTable dt = new DataTable();
                dt = clsDb.retorna_DataTable(cmd);

                string idAp = dt.Rows[0][0].ToString();
                return idAp;
            }

            catch (Exception ex) {
                               
                return "";
            }
        }

        public bool gravarMoradorUnidade(string nome_pessoa, string rg, string cpf, string cnpj, string ddd, string telefone, string biometria_polegar, string biometria_indicador, string biometria_medio, int tipo_pessoa, string observacao, int id_unidade) {

            comando = "INSERT INTO PESSOA (Nome,Rg,Cpf, Cnpj, Ddd, Telefone, BiometriaPolegar, BiometriaIndicador,BiometriaMedio, TipoPessoa) VALUES ('" + nome_pessoa + "', '" + rg + "', '" + cpf + "', '" + cnpj + "', '" + ddd + "', '" + telefone + "', '" + biometria_polegar + "', '" + biometria_indicador + "', '" + biometria_medio + "', '" + Convert.ToString(tipo_pessoa) + "');";
                      
            if (clsDb.executa_sql(comando))
            {
                comando = "SELECT LAST_INSERT_ID();";
                int id = clsDb.retorna_Ultimo_Id(comando);

                if (id > -1)
                {
                    comando = "INSERT INTO MORADORUNIDADECONDOMINIAL(OBSERVACAO, PESSOA_ID, UNIDADECONDOMINIAL_ID) VALUES('" + observacao + "', "+id+ ", " + id_unidade + ");";
                    clsDb.executa_sql(comando);
                    return true;
                }
                else return false;
                
            }
            else return false;
        }


        public void comandosCarregarCbxBloco(ref ComboBox cbxBloco)
        {
            comando = "SELECT PREDIO FROM UNIDADECONDOMINIAL GROUP BY PREDIO;";
            string dm = "PREDIO";
            clsDb.FCarregar_Cbx(ref cbxBloco, comando, dm);
        }

        public void comandosCarregarCbxNumAp(ref ComboBox cbxNumAp, string predio, string escada)
        {
            comando = "SELECT NUMEROAPARTAMENTO FROM UNIDADECONDOMINIAL WHERE PREDIO = '"+predio+"' AND ESCADA = '"+escada+"'";
            string dm = "NUMEROAPARTAMENTO";
            clsDb.FCarregar_Cbx(ref cbxNumAp, comando, dm);
        }

    }
}
