﻿using System;
using System.Collections.Generic;

namespace easyAccess.Models
{
    public partial class UnidadeCondominial
    {
        public long Id { get; set; }
        public string Predio { get; set; }
        public string Escada { get; set; }
        public string Andar { get; set; }
        public string NumeroApartamento { get; set; }
        public string ObservacoesLocalizacao { get; set; }
    }
}
