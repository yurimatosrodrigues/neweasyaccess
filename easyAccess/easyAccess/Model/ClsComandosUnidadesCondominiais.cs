﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace easyAccess.Model
{
    public class ClsComandosUnidadesCondominiais
    {

        ClsDb clsDb = new ClsDb();
        string comandos = "";

        public bool consultarUnidades(ref DataTable dt) {

            try
            {
                comandos = "SELECT ID, PREDIO AS 'BLOCO', ESCADA, NUMEROAPARTAMENTO AS 'NUM. AP', ANDAR FROM UNIDADECONDOMINIAL";

                MySqlCommand cmd = new MySqlCommand(comandos);
                cmd.CommandType = CommandType.Text;

                dt = clsDb.retorna_DataTable(cmd);
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro na consulta! =( - " + ex.Message, "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }


        }

    }
}
