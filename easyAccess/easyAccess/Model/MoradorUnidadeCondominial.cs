﻿using System;
using System.Collections.Generic;

namespace easyAccess.Models
{
    public partial class MoradorUnidadeCondominial
    {
        public long Id { get; set; }
        public string Observacao { get; set; }
        public long PessoaId { get; set; }
        public long UnidadeCondominialId { get; set; }
    }
}
