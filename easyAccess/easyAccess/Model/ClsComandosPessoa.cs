﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace easyAccess.Model
{
    public class ClsComandosPessoa
    {
                
        ClsDb clsDb = new ClsDb();
        string comando;

        //public bool comandosGravarPessoa(string nome_pessoa, string rg, string cpf, string cnpj, string ddd, string telefone, string email, string biometria_polegar, string biometria_indicador, string biometria_medio, int tipo_pessoa)
        //{

        //    comando = "INSERT INTO PESSOA (Nome,Rg,Cpf, Cnpj, Ddd, Telefone, Email, BiometriaPolegar, BiometriaIndicador,BiometriaMedio, TipoPessoa) VALUES ('" + nome_pessoa+"',   '"+rg+"', '"+cpf+"', '"+cnpj+"', '"+ddd+"', '"+telefone+ "', '"+email+"' '" + biometria_polegar + "', '" + biometria_indicador + "', '" + biometria_medio+"', '"+ Convert.ToString(tipo_pessoa)+"');";

        //    if (clsDb.executa_sql(comando))
        //        return true;
        //    else
        //        return false;           
        //}


        public bool comandosGravarPessoa(string nome_pessoa, string rg, string cpf, string cnpj, string ddd, string telefone, string email, string biometria_polegar, string biometria_indicador, string biometria_medio, int tipo_pessoa)
        {
                     
            MySqlCommand cmd = new MySqlCommand();

            cmd.CommandText = "INSERT INTO PESSOA (Nome,Rg,Cpf, Cnpj, Ddd, Telefone, Email, BiometriaPolegar, BiometriaIndicador,BiometriaMedio, TipoPessoa) VALUES (@nome_pessoa, @rg, @cpf, @cnpj, @ddd, @telefone, @email, @biometria_polegar, @biometria_indicador, @biometria_medio, @tipo_pessoa);";           
            cmd.Parameters.AddWithValue("@nome_pessoa", nome_pessoa);
            cmd.Parameters.AddWithValue("@rg", rg);
            cmd.Parameters.AddWithValue("@cpf", cpf);
            cmd.Parameters.AddWithValue("@cnpj", cnpj);
            cmd.Parameters.AddWithValue("@ddd", ddd);
            cmd.Parameters.AddWithValue("@telefone", telefone);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@biometria_polegar", biometria_polegar);
            cmd.Parameters.AddWithValue("@biometria_indicador", biometria_indicador);
            cmd.Parameters.AddWithValue("@biometria_medio", biometria_medio);
            cmd.Parameters.AddWithValue("@tipo_pessoa", tipo_pessoa);
                         

            if (clsDb.insereRegistros(cmd))
                return true;
            else
                return false;
        }




        public bool comandosConsultarPessoa(int tipo_pessoa, ref DataTable dtbPessoa)
        {

            try
            {
                comando = "SELECT CASE WHEN TIPOPESSOA = '1' THEN 'Morador' WHEN TIPOPESSOA = '2' THEN 'Visitante' WHEN TIPOPESSOA = '3' THEN 'Prestador de Serviço' ELSE '' END AS 'Tipo Pessoa', NOME, RG, CPF, CNPJ, DDD, TELEFONE FROM PESSOA";

                if (tipo_pessoa > -1)
                    comando = comando + " WHERE TIPOPESSOA = @tipo_pessoa ORDER BY TIPOPESSOA";

                MySqlCommand cmd = new MySqlCommand(comando);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@tipo_pessoa", tipo_pessoa);

                dtbPessoa = clsDb.retorna_DataTable(cmd);


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro na consulta! =( - " + ex.Message, "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            
        }

        public bool comandosConsultarPessoa(int tipo_pessoa, ref DataTable dtbPessoa, string cpf, string rg)
        {
                                   
            comando = "SELECT ID, NOME, RG, CPF, CNPJ, DDD, TELEFONE FROM PESSOA";            
            
            if (tipo_pessoa > -1) {

                if (tipo_pessoa == 2) 
                    comando = comando + " WHERE (CPF = @Cpf OR RG = @Rg)";                    
                
                else if(tipo_pessoa == 3)
                    comando = comando + " WHERE (CNPJ = @Cpf OR RG = @Rg)";

                comando = comando + " AND TIPOPESSOA = @tipo_pessoa ORDER BY TIPOPESSOA";
            }
                          
                        

            MySqlCommand cmd = new MySqlCommand(comando);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@tipo_pessoa", tipo_pessoa);
            cmd.Parameters.AddWithValue("@Cpf", cpf);
            cmd.Parameters.AddWithValue("@Rg", rg);

            dtbPessoa = clsDb.retorna_DataTable(cmd);


            return true;
        }

        public void comandosCarregarCbxTipoPessoa(ref ComboBox cbxTipoPessoa)
        {
            comando = "SELECT TIPOPESSOA FROM PESSOA";
            string dm = "TIPOPESSOA";
            clsDb.FCarregar_Cbx(ref cbxTipoPessoa, comando, dm);

        }
    }
}
