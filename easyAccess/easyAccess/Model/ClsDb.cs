﻿using System;

using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;

namespace easyAccess.Model
{
    public class ClsDb
    {
        string conexao = "";
        string comandos;

        MySqlConnection con = new MySqlConnection();
        MySqlCommand cmd;        

        public ClsDb(){
            conexao = retornaStringConexao();
            con.ConnectionString = conexao;
            //con.ConnectionString = @"Server=localhost;Database=easyaccess;userid=root;IntegratedSecurity=yes";
        }

        private string retornaStringConexao()
        {
            string conexao = "";

            if (File.Exists(Application.StartupPath + "//string.txt"))
            {

                StreamReader SR = new StreamReader(Application.StartupPath + "//string.txt");
                conexao = SR.ReadToEnd();

                if (conexao == "")
                    MessageBox.Show("String de conexão vazia!", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else
                MessageBox.Show("Arquivo de String de conexão não existe!", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
            return conexao;
        }

        public MySqlConnection conectar() {

            if (con.State == System.Data.ConnectionState.Closed) {
                con.Open();
            }

            return con;

        }

        public void desconectar() {
            if (con.State == System.Data.ConnectionState.Open)
            {
                con.Close();
            }
        }


        public bool executa_sql(string str_sql) {

            try
            {
                if (str_sql != String.Empty)
                    comandos = str_sql;

                conectar();

                cmd = new MySqlCommand(comandos, con);
                cmd.CommandTimeout = 40000000;
                cmd.ExecuteNonQuery();
                
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro no comando SQL. executa_sql - ClsDb. " + ex.Message + ex.Data + ex.InnerException, "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            finally
            {
                desconectar();
            }
            
        }

        public bool insereRegistros(MySqlCommand cmd)
        {
            try
            {
                conectar();
                cmd.Connection = con;
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro no insert do registro. insereRegistros - ClsDb. " + ex.Message + ex.Data + ex.InnerException, "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            finally
            {
                desconectar();
            }
        }

        public DataTable retorna_DataTable(MySqlCommand cmd)
        {
            DataTable dt = new DataTable();
            MySqlDataAdapter sda = new MySqlDataAdapter();
            
            try
            {
                conectar();

                cmd.Connection = con;
                sda.SelectCommand = cmd;
                sda.Fill(dt);
                
                return dt;  

            }
            catch (Exception ex)
            {

                MessageBox.Show("Erro - ClsDb - DataTable return. " + ex.Message + ex.Source, "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return dt;
            }
            finally
            {
                desconectar();
                sda.Dispose();
            }
        }

        public bool FCarregar_Cbx(ref ComboBox combo, String str_SQL, string dm)
        {
            try
            {

                string m_StrSQL = "";

                if (str_SQL != String.Empty)
                    m_StrSQL = str_SQL;

                conectar();

                MySqlCommand m_Comando = new MySqlCommand(m_StrSQL, con);

                MySqlDataReader dr = m_Comando.ExecuteReader();

                DataTable dt = new DataTable("Combo");
                dt.Load(dr);

                combo.DisplayMember = dm;
                combo.DataSource = dt;
                
                
                                            
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Ocorreu um erro ao popular o COMBO BOX: Número do Erro: " + ex.ErrorCode + "Descrição do Erro  : " + ex.Message + "FCarregar_Cbx");
                return false;
            }

            finally
            {
                desconectar();
            }

        }


        public int retorna_Ultimo_Id(string str_sql)
        {

            try
            {
                if (str_sql != String.Empty)
                    comandos = str_sql;

                conectar();

                cmd = new MySqlCommand(comandos, con);
                cmd.CommandTimeout = 40000000;
                IDataReader reader = cmd.ExecuteReader();

                int id = 0;

                if (reader != null && reader.Read())
                    id = reader.GetInt16(0);
                else
                    id = -1;

                reader.Close();
                reader.Dispose();
                reader = null;

                return id;

            }
            catch (Exception ex)
            {
                //throw;
                //MENSAGEM DE ERRO
                MessageBox.Show("Erro no INSERT do registro. executa_sql - ClsDb. " + ex.Message + ex.Data + ex.InnerException, "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1;
            }
            finally
            {
                desconectar();
            }
        }
    }
}
