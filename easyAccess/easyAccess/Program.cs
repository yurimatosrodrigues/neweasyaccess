﻿using easyAccess.Models;
using easyAccess.Network;
using easyAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace easyAccess
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            #region Testes

            //EasyAccessAPI api = new EasyAccessAPI("usuarios");

            //List<Usuario> usuarios = api.Get<List<Usuario>>().Result;

            //Console.WriteLine(usuarios[0].Email);

            #endregion

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new FrmLogin());
        }
    }
}
