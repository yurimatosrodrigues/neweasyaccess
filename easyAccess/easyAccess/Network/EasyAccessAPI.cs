﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace easyAccess.Network
{
    class EasyAccessAPI
    {
        private const string BaseUrl = "http://localhost:54029/api/";

        private readonly HttpClient http;
        private readonly string uri;

        public EasyAccessAPI(string uri)
        {
            http = new HttpClient();
            this.uri = uri;

            http.BaseAddress = new Uri(BaseUrl);
            http.DefaultRequestHeaders.Accept.Clear();
            http.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<ResponseType> Get<ResponseType>()
        {
            HttpResponseMessage response = await http.GetAsync(uri);

            response.EnsureSuccessStatusCode();

            ResponseType responseObject = await response.Content.ReadAsAsync<ResponseType>();

            return responseObject;
        }
    }
}
