﻿using easyAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace easyAccess.Controller
{
    public class ClsLogin
    {

        public bool tem;
        public String mensagem = "";

        private int lId;
        private string lUsuario;
        private string lSenha;

        public int Id{ get { return lId; } set { lId = value; } }

        public string Usuario{ get { return lUsuario; } set { lUsuario = value; } }

        public string Senha { get { return lSenha;  } set { lSenha = value; } }

        public bool acessar() {
                   
            ClsComandosLogin clsComandosLogin = new ClsComandosLogin();

            tem = clsComandosLogin.verificarLogin(ref lId, lUsuario, lSenha);

            //Verifica se ocorreu algum erro
            if (!clsComandosLogin.mensagem.Equals("")){
                this.mensagem = clsComandosLogin.mensagem;
            }
                   
            return tem;
        }

    }
}
