﻿using easyAccess.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace easyAccess.Controller
{
    public class ClsUnidadesCondominiais
    {

        ClsComandosUnidadesCondominiais clsComandosUnidadesCondominiais = new ClsComandosUnidadesCondominiais();

        public bool consultarUnidades(ref DataTable dt) {
            if (clsComandosUnidadesCondominiais.consultarUnidades(ref dt))
                return true;
            else
                return false;
        }

    }
}
