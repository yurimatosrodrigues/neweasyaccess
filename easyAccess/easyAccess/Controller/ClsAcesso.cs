﻿using easyAccess.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace easyAccess.Controller
{
    public class ClsAcesso
    {
        
        #region Variaveis

        ClsComandosAcesso clsComandosAcesso = new ClsComandosAcesso();
        ClsComandosPessoa clsComandosPessoa = new ClsComandosPessoa();
        ClsComandosMoradorUnidade clsComandosMoradorUnidade = new ClsComandosMoradorUnidade();
        
        int lId_Pessoa = -1;
        string lNome_pessoa;
        string lRg;
        string lCpf;
        string lCpfCnpj;
        string lTelefone;
 
        string lBloco2;
        string lEscada2;
        string lNumeroAp2;
        string lNome3;

        int lIdUnidade = -1;
        int lIdUsuario;
        string lObservacao;

        #endregion
                
        #region Propriedades

        public int IdPessoa { get { return lId_Pessoa; } set { lId_Pessoa = value; } }
        public string Nome {get { return lNome_pessoa; } set { this.lNome_pessoa = value; } }
        public string Rg { get { return lRg; } set { this.lRg = value.Replace(".", "").Replace("-", ""); } }
        public string Cpf { get { return lCpf; } set { this.lCpf = value.Replace(".", "").Replace("-", ""); ; } }
        public string CpfCnpj{ get { return lCpfCnpj; } set { lCpfCnpj = value.Replace(".", "").Replace("-", "").Replace("/",""); } }
        public string Telefone { get { return lTelefone; } set { this.lTelefone = value.Replace("-", "").Replace(" ", ""); } }

        public string Bloco2 { get { return lBloco2; } set { lBloco2 = value; } }
        public string Escada2 { get { return lEscada2; } set { lEscada2 = value; } }
        public string NumeroAp2 { get { return lNumeroAp2; } set { lNumeroAp2 = value; } }
        public string Nome3 { get { return lNome3; } set { lNome3 = value; } }

        public int IdUnidade { get { return lIdUnidade; } set { lIdUnidade = value; } }
        public int IdUsuario { get { return lIdUsuario; } set { lIdUsuario = value; } }
        public string Observacao { get { return lObservacao; } set { lObservacao = value; } }



        #endregion


        public bool consultarAcessos(ref DataTable dtAcessos) {

            if (clsComandosAcesso.consultarAcessos(ref dtAcessos))
                return true;
            else
                return false;
        }

        public bool consultarAcessos(ref DataTable dtAcessos, DateTime data, string tipoPessoa, string dataExata, string bloco, string escada, string numap)
        {

            string tipPes = "";

            if (tipoPessoa == "Morador")
                tipPes = "1";
            else if (tipoPessoa == "Visitante")
                tipPes = "2";
            else if (tipoPessoa == "Prestador de Serviço")
                tipPes = "3";
            else
                tipPes = "";


            if (clsComandosAcesso.consultarAcessos(ref dtAcessos, data, tipPes, dataExata, bloco, escada, numap))
                return true;
            else
                return false;
        }


        public bool consultarMorador() {

            DataTable dt = new DataTable();
            clsComandosAcesso.consultarMorador(lCpf, lRg, ref dt);

            //bloquear cadastro de Pessoa com o mesmo CPF
            try
            {
                lId_Pessoa = Convert.ToInt16(dt.Rows[0][0].ToString());
                lNome_pessoa = dt.Rows[0][1].ToString();
                lCpf = dt.Rows[0][2].ToString();
                lRg = dt.Rows[0][3].ToString();
                lTelefone = dt.Rows[0][4].ToString();

                lBloco2 = dt.Rows[0][5].ToString();
                lEscada2 = dt.Rows[0][6].ToString();
                lNumeroAp2 = dt.Rows[0][7].ToString();
                lIdUnidade = Convert.ToInt16(dt.Rows[0][8].ToString());

                lNome3 = dt.Rows[0][1].ToString();  

                return true;
            }
            catch (Exception ex){

                MessageBox.Show("Nenhum morador encontrado! =(", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            
        }

        public bool consultarVisitante() {

            try
            {
                DataTable dtVisitante = new DataTable();
                clsComandosPessoa.comandosConsultarPessoa(2, ref dtVisitante, lCpf, lRg);

                lId_Pessoa = Convert.ToInt16(dtVisitante.Rows[0][0].ToString());
                lNome_pessoa = dtVisitante.Rows[0][1].ToString();
                lRg = dtVisitante.Rows[0][2].ToString();
                lCpf = dtVisitante.Rows[0][3].ToString();
                lTelefone = dtVisitante.Rows[0][6].ToString();

                return true;
            }
            catch (Exception)
            {

                MessageBox.Show("Nenhum visitante encontrado! =(", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        public bool consultarPS()
        {
            try
            {
                DataTable dtPs = new DataTable();
                clsComandosPessoa.comandosConsultarPessoa(3, ref dtPs, lCpfCnpj, lRg);

                lId_Pessoa = Convert.ToInt16(dtPs.Rows[0][0].ToString());
                lNome_pessoa = dtPs.Rows[0][1].ToString();
                lRg = dtPs.Rows[0][2].ToString();
                lCpfCnpj = dtPs.Rows[0][4].ToString();
                lTelefone = dtPs.Rows[0][6].ToString();
                
                return true;
            }
            catch (Exception)
            {

                MessageBox.Show("Nenhum prestador de serviço encontrado! =(", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        public void pegaIdUnidade(string bloco, string escada, string numeroap) {
            DataTable dtIdUnidade = new DataTable();
            clsComandosAcesso.pegaIdUnidade(ref dtIdUnidade, bloco, escada, numeroap);
            lIdUnidade = Convert.ToInt16(dtIdUnidade.Rows[0][0].ToString());
        }
        
        public bool registrarAcesso() {

            if (lId_Pessoa == -1 || lIdUnidade == -1) {
                MessageBox.Show("Por favor, selecione uma pessoa e uma unidade para registrar o acesso! =)", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }                
            else {
                if (clsComandosAcesso.registrarAcesso(lObservacao, lId_Pessoa, lIdUsuario, lIdUnidade))
                {
                    MessageBox.Show("Acesso registrado com sucesso! =)", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpaRegistros();
                    return true;
                }

                else {
                    MessageBox.Show("Erro ao registrar acesso! =(", "Easy Access", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }                    
            }
        }

        
        public void carregarComboBloco(ref ComboBox cbxBloco) {
            clsComandosAcesso.comandosCarregarCbxBloco(ref cbxBloco);
        }

        public void carregarComboNumAp(ref ComboBox cbxNumAp, string bloco, string escada) {
            clsComandosAcesso.comandosCarregarCbxNumAp(ref cbxNumAp, bloco, escada);
        }

        private void limpaRegistros() {
            
            lId_Pessoa = -1;
            lNome_pessoa = "";
            lRg = "";
            lCpf = "";
            lCpfCnpj = "";
        }

    }
}
