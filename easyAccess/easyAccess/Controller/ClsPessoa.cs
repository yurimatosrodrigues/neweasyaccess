﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using easyAccess.Model;
using static easyAccess.ClsUtil;
using System.Data;
using System.Windows.Forms;
using System.IO;

namespace easyAccess.Controller
{
    public class ClsPessoa
    {

        ClsComandosPessoa clsComandosPessoa = new ClsComandosPessoa();
        
        #region Variaveis Locais

        private string lStrTipo_Pessoa;

        private int lIntTipo_pessoa;
        private string lNome_pessoa;
        private string lRg;
        private string lCpf;
        private string lCnpj;
        private string lDdd;
        private string lTelefone;
        private string lEmail;
        private string lBiometria_polegar;
        private string lBiometria_indicador;
        private string lBiometria_medio;   
        #endregion


        #region Propriedades

        public string Tipo_Pessoa_Str{
            get {
                return lStrTipo_Pessoa;
            }
            set {
                lStrTipo_Pessoa = value;
            }
        }

        public int Tipo_Pessoa_Int
        {
            get
            {
                return lIntTipo_pessoa;
            }
            set
            {
                this.lIntTipo_pessoa = value;
            }
        }

        public string Nome
        {
            get
            {
                return lNome_pessoa;
            }
            set
            {
                this.lNome_pessoa = value;
            }
        }

      
        public string Rg
        {

            get
            {
                return lRg;
            }
            set
            {
                this.lRg = value.Replace(".","").Replace("-","");
            }
        }

        public string Cpf
        {
            get
            {
                return lCpf;
            }
            set
            {                
                this.lCpf = value.Replace(".", "").Replace("-", ""); ;
            }
        }

        public string Cnpj
        {
            get
            {
                return lCnpj;
            }
            set
            {
                this.lCnpj = value.Replace(".", "").Replace("-", "").Replace("/", "");
            }
        }

        public string Ddd
        {
            get
            {
                return lDdd;
            }
            set
            {
                this.lDdd = value.Replace("(", "").Replace(")", "");
            }
        }

        public string Telefone
        {
            get
            {
                return lTelefone;
            }
            set
            {
                this.lTelefone = value.Replace("-", "").Replace(" ", "");
            }
        }

        public string Email { get { return lEmail; } set { lEmail = value; } }

        public string Biometria_Polegar {
            get {
                return lBiometria_polegar;
            }
            set {
                lBiometria_polegar = value;
            }
        }
        public string Biometria_Indicador
        {
            get
            {
                return lBiometria_indicador;
            }
            set
            {
                this.lBiometria_indicador = value;
            }
        }

        public string Biometria_Medio
        {
            get
            {
                return lBiometria_medio;
            }
            set
            {
                this.lBiometria_medio = value;
            }
        }


        #endregion
        

        public bool gravarPessoa(bool adicionando){
            
            verfificaTipoPessoa();

            if (clsComandosPessoa.comandosGravarPessoa(lNome_pessoa, lRg, lCpf, lCnpj, lDdd, lTelefone, lEmail, lBiometria_polegar, lBiometria_indicador, lBiometria_medio, lIntTipo_pessoa))
                return true;
            else
                return false;              
        }
        
        
        public bool consultarPessoa(ref DataTable dt){
            verfificaTipoPessoa();
            clsComandosPessoa.comandosConsultarPessoa(lIntTipo_pessoa, ref dt);
            return true;

        }

        public void carregarCbxTipoPessoa(ref ComboBox cbxTipoPessoa)
        {
            clsComandosPessoa.comandosCarregarCbxTipoPessoa(ref cbxTipoPessoa);            
        }


        public void verfificaTipoPessoa() {


            if (lStrTipo_Pessoa == "Morador")
                lIntTipo_pessoa = (int)Tipo_Pessoa.Morador;
            else if (lStrTipo_Pessoa == "Visitante")
                lIntTipo_pessoa = (int)Tipo_Pessoa.Visitante;
            else if (lStrTipo_Pessoa == "Prestador de Serviço")
                lIntTipo_pessoa = (int)Tipo_Pessoa.PrestadorServico;     
            else
                lIntTipo_pessoa = -1;
        }
        
    }
}
