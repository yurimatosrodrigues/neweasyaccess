﻿using easyAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace easyAccess.Controller
{
    public class ClsMoradorUnidade
    {
        
        ClsComandosMoradorUnidade clsComandosMoradorUnidade = new ClsComandosMoradorUnidade();

        string idAp = "";
        string lObservacao;
        int lIdAp;

        public string Observavao{
            get {
                return lObservacao;
            }
            set {
                lObservacao = value;
            }
        }

        public int IdAp{ get { return lIdAp; } set { lIdAp = value; } }
               

        public string buscarIdAp(string bloco, string escada, string numeroAp)
        {            
            idAp = clsComandosMoradorUnidade.comandosConsultarIdAp(bloco, escada, numeroAp);
            return idAp;
        }

        public bool gravarMoradorUnidade(ClsPessoa clsPessoa)
        {
            clsPessoa.verfificaTipoPessoa();
            return clsComandosMoradorUnidade.gravarMoradorUnidade(clsPessoa.Nome, clsPessoa.Rg, clsPessoa.Cpf, clsPessoa.Cnpj, clsPessoa.Ddd, clsPessoa.Telefone, clsPessoa.Biometria_Polegar, clsPessoa.Biometria_Indicador, clsPessoa.Biometria_Medio, clsPessoa.Tipo_Pessoa_Int, lObservacao, lIdAp);
        }

        public void carregarComboBloco(ref ComboBox cbxBloco) {
            clsComandosMoradorUnidade.comandosCarregarCbxBloco(ref cbxBloco);
        }

        public void carregarComboNumAp(ref ComboBox cbxNumAp, string predio, string escada){
            clsComandosMoradorUnidade.comandosCarregarCbxNumAp(ref cbxNumAp, predio, escada);
        }
        
     }
}

